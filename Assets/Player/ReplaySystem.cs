﻿using UnityEngine;
using System.Collections;

public class ReplaySystem : MonoBehaviour {

	public GameManager gameManager;

	private const int bufferFrames = 100;
	private MyKeyFrame [] keyFrames = new MyKeyFrame [bufferFrames];

	private Rigidbody rigidbody;

	void Start () {
		MyKeyFrame testKeyFrame = new MyKeyFrame (1.0f, Vector3.up, Quaternion.identity);
		rigidbody = GetComponent<Rigidbody> ();
		gameManager = FindObjectOfType<GameManager> ();
	}

	void Update () {
		if (gameManager.isRecording) {
			Record ();
		} else PlayBack ();
	}

	public void Record () {
		rigidbody.isKinematic = false;
		float time = Time.time;
		int frame = Time.frameCount % bufferFrames;
		//print ("Writing frame " + frame);

		keyFrames [frame] = new MyKeyFrame (time, transform.position, transform.rotation);
	}

	public void PlayBack () {
		rigidbody.isKinematic = true;
		int frame = Time.frameCount % bufferFrames;
		//print ("Reading frame " + frame);
		transform.position = keyFrames [frame].position;
		transform.rotation = keyFrames [frame].rotation;
	}
}

/// <summary>
/// A structure for storing time, position and rotation
/// </summary>
public struct MyKeyFrame {

	public float frameTime;
	public Vector3 position;
	public Quaternion rotation;

	// Constructor
	public MyKeyFrame (float frame, Vector3 pos, Quaternion rot) {
		frameTime = frame;
		position = pos;
		rotation = rot;
	}
}
