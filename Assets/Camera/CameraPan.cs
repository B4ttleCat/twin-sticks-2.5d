﻿using UnityEngine;
using System.Collections;

public class CameraPan : MonoBehaviour {

	Player player;

	void Start () {
		player = FindObjectOfType<Player> ();
	}
	
	
	void LateUpdate () {
		transform.LookAt (player.transform.position);
	}
}
