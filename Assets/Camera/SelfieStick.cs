﻿using UnityEngine;
using System.Collections;

public class SelfieStick : MonoBehaviour {

	Player player;
	Vector3 armRotation;

	public float panSpeed = 10f;


	void Start () {
		player = FindObjectOfType<Player> ();
		armRotation = transform.rotation.eulerAngles;
	}
	
	
	void Update () {
		armRotation.x += Input.GetAxis ("RVert") * panSpeed;
		armRotation.y += Input.GetAxis ("RHoriz") * panSpeed;
		transform.rotation = Quaternion.Euler (armRotation);

		transform.position = player.transform.position;
	}
}
